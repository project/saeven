<?php

/**
 * @file
 * Contains the theme function override for 'breadcrumb'.
 */

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @ingroup themeable
 */
function saeven_breadcrumb($variables) {
  return theme_breadcrumb($variables);
}
