<?php

/**
 * @file
 * Contains a pre-process hook for 'page'.
 */

/**
 * Implements hook_preprocess_TEMPLATE().
 */
function saeven_preprocess_page(&$variables) {
  $variables['primary_local_tasks'] = $variables['tabs'];
  unset($variables['primary_local_tasks']['#secondary']);
  $variables['secondary_local_tasks'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $variables['tabs']['#secondary'],
  );

  // Prepare the svg URL
  if (strpos($variables['logo'], 'eldir')) {
    $variables['svg_logo'] = str_replace('logo.png', 'images-source/aegir_logo_horizontal.svg', $variables['logo']);
  }

  // Overlay is enabled.
  $variables['overlay'] = (module_exists('overlay') && overlay_get_mode() === 'child');

  $variables['tabs2'] = $variables['tabs'];
  unset($variables['tabs']['#secondary']);
  unset($variables['tabs2']['#primary']);

  // Move the local actions into place of the secondary tabs, for now.
  /*
  if (is_array($variables['action_links'])) {
    foreach ($variables['action_links'] as $link) {
      $variables['tabs2']['#secondary'][] = $link;
      $variables['tabs2']['#theme'] = 'menu_local_tasks';
    }
    $variables['action_links']['#access'] = FALSE;
  }
  */

  if (!isset($variables['title'])) {
    $variables['title'] = isset($variables['node']) ? $variables['node']->title : drupal_get_title();
  }

  $node = menu_get_object();
  if (!empty($node) && !$variables['overlay']) {
    // Add a node type label on node pages to help users.
    $types = node_type_get_types();
    $type = $node->type;
    if (!empty($types[$type])) {
      $variables['title'] = "<span class='label'>{$types[$type]->name}</span>" . $variables['title'];

    }
  }
}
