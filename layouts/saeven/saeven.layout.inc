name = Saeven default layout
description = A 3 column layout.
preview = preview.png
template = saeven-layout

; Regions
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/saeven/saeven-normalize.css
stylesheets[all][] = css/layouts/saeven/saeven.layout.css
stylesheets[all][] = css/layouts/saeven/saeven.layout.no-query.css
stylesheets[all][] = style/css/misc/style.css
