name = Saeven layout for Aegir
description = A 3 column layout.
preview = preview.png
template = aegir-layout

; Regions
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/aegir/saeven-normalize.css
stylesheets[all][] = css/layouts/aegir/saeven.aegir.css
stylesheets[all][] = css/layouts/aegir/saeven.layout.css
stylesheets[all][] = css/layouts/aegir/saeven.layout.no-query.css
stylesheets[all][] = style/css/misc/style.css

